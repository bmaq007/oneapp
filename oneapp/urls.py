from django.conf.urls import patterns, include, url
from django.contrib import admin
from oneapp import views, social_media_views


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^$', views.index, name='index'),
    url(r'^$', views.myposts, name='my_posts'),
    url(r'^login', views.user_login, name='user_login'),
    url(r'^logout/$', views.user_logout, name='user_logout'),
    url(r'^register', views.create_account, name='create_account'),
    url(r'^widgets/$', social_media_views.widgets, name='widgets'),
    url(r'^twitter/account_id/(?P<account_id>[0-9]+)', social_media_views.twitter_widget, name='twitter_widget'),
	url(r'^instagram/account_id/(?P<account_id>[0-9]+)', social_media_views.instagram_widget, name='instagram_widget'),
	url(r'^facebook/account_id/(?P<account_id>[0-9]+)', social_media_views.facebook_widget, name='facebook_widget'),
    url(r'^post/$', social_media_views.post_widget, name='social_post'),
    url(r'^my/twitter/account_id/(?P<account_id>[0-9]+)', social_media_views.my_twitter_widget, name='my_twitter_widget'),
	url(r'^my/instagram/account_id/(?P<account_id>[0-9]+)', social_media_views.my_instagram_widget, name='my_instagram_widget'),
	url(r'^my/facebook/account_id/(?P<account_id>[0-9]+)', social_media_views.my_facebook_widget, name='my_facebook_widget'),

)
