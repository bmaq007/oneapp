from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from allauth.socialaccount.models import SocialApp, SocialToken, SocialAccount
from twython import Twython
from instagram import client
from open_facebook import OpenFacebook
from dateutil import parser as dateparser
import datetime
import pytz


def widgets(request):
    return HttpResponse('blah blah blah')


@login_required()
def twitter_widget(request, account_id):
    #check if this account belongs to the logged in user
    belongs = SocialAccount.objects.get(id=account_id, user_id=request.user.id)
    if belongs:
        app = SocialApp.objects.get(provider='twitter')
        #Check if Twitter Provider is configured
        if app:
            token = SocialToken.objects.get(app_id=app.id, account_id=account_id)
            #check if Twitter Auth token objects are configured for this account
            if token:
                #create the twitter object from twython
                twitter = Twython(app.client_id, app.secret, token.token, token.token_secret)
                timeline = twitter.get_home_timeline()
                context = {
                    'timeline' : timeline
                }
                return render(request, 'twitter_widget.html', context)
        #not configured
        else:
            return HttpResponse("Twitter Provider is not Configured")
    #account does not belong to the logged in user
    else:
        return HttpResponse("You are not allowed to view this Account")


@login_required
def instagram_widget(request, account_id):
    # check if this account belongs to the logged in user
    belongs = SocialAccount.objects.get(id=account_id, user_id=request.user.id)
    if belongs:
        app = SocialApp.objects.get(provider='instagram')
        # Check if Instagram Provider is configured
        if app:
            token = SocialToken.objects.get(app_id=app.id, account_id=account_id)
            instagram_client = client.InstagramAPI(access_token=token.token, client_secret='')
            # get users that this user follows
            listofobjects = []
            follows = instagram_client.user_follows(user_id=belongs.uid)
            # get media for each followed user
            for follow in follows[0]:
                recent_media = instagram_client.user_recent_media(user_id=follow.id, count=20)
                for media in recent_media[0]:
                    d = {
                        'photo': media.images['standard_resolution'].url,
                        'created_date': human_readable_time(media.created_time.replace(tzinfo = pytz.utc)),#media.created_time,
                        'poster': follow.username,
                        'comments_count': media.comments.count,
                    }
                listofobjects.append(d)
                context = {'timeline': listofobjects}
            return render(request, 'instagram_widget.html', context)
        #not configured
        else:
            return HttpResponse("Instagram Provider is not Configured")
    #account does not belong to the logged in user
    else:
        return HttpResponse("You are not allowed to view this Account")


@login_required
def facebook_widget(request, account_id):
    # check if this account belongs to the logged in user
    belongs = SocialAccount.objects.get(id=account_id, user_id=request.user.id)
    if belongs:
        app = SocialApp.objects.get(provider='facebook')
        # Check if Facebook Provider is configured
        if app:
            token = SocialToken.objects.get(app_id=app.id, account_id=account_id)
            facebook = OpenFacebook(access_token=token.token)
            data = mine_facebook_data(facebook.get('me/home'))
            context = {
                    'timeline' : data
                }
            return render(request, 'facebook_widget.html', context)
        #not configured
        else:
            return HttpResponse("Facebook Provider is not Configured")
    #account does not belong to the logged in user
    else:
        return HttpResponse("You are not allowed to view this Account")


@login_required()
def my_twitter_widget(request, account_id):
    #check if this account belongs to the logged in user
    belongs = SocialAccount.objects.get(id=account_id, user_id=request.user.id)
    if belongs:
        app = SocialApp.objects.get(provider='twitter')
        #Check if Twitter Provider is configured
        if app:
            token = SocialToken.objects.get(app_id=app.id, account_id=account_id)
            #check if Twitter Auth token objects are configured for this account
            if token:
                #create the twitter object from twython
                twitter = Twython(app.client_id, app.secret, token.token, token.token_secret)
                timeline = twitter.get_user_timeline()
                context = {
                    'timeline' : timeline
                }
                return render(request, 'twitter_widget.html', context)
        #not configured
        else:
            return HttpResponse("Twitter Provider is not Configured")
    #account does not belong to the logged in user
    else:
        return HttpResponse("You are not allowed to view this Account")


@login_required
def my_instagram_widget(request, account_id):
    # check if this account belongs to the logged in user
    belongs = SocialAccount.objects.get(id=account_id, user_id=request.user.id)
    if belongs:
        app = SocialApp.objects.get(provider='instagram')
        # Check if Instagram Provider is configured
        if app:
            token = SocialToken.objects.get(app_id=app.id, account_id=account_id)
            instagram_client = client.InstagramAPI(access_token=token.token, client_secret='')
            # get users that this user follows
            recent_media = instagram_client.user_recent_media(user_id=belongs.uid, count=30)
            listofobjects = []
            for media in recent_media[0]:
                d = {
                    'photo': media.images['standard_resolution'].url,
                    'created_date': human_readable_time(media.created_time.replace(tzinfo = pytz.utc)),
                    'poster': belongs.user,
                    'comments_count': media.comments.count,
                }
                listofobjects.append(d)
            context = {'timeline': listofobjects}
            return render(request, 'instagram_widget.html', context)
        #not configured
        else:
            return HttpResponse("Instagram Provider is not Configured")
    #account does not belong to the logged in user
    else:
        return HttpResponse("You are not allowed to view this Account")


@login_required
def my_facebook_widget(request, account_id):
    # check if this account belongs to the logged in user
    belongs = SocialAccount.objects.get(id=account_id, user_id=request.user.id)
    if belongs:
        app = SocialApp.objects.get(provider='facebook')
        # Check if Facebook Provider is configured
        if app:
            token = SocialToken.objects.get(app_id=app.id, account_id=account_id)
            facebook = OpenFacebook(access_token=token.token)
            data = mine_facebook_data(facebook.get('me/feed'))
            context = {
                    'timeline' : data
                }
            return render(request, 'facebook_widget.html', context)
        #not configured
        else:
            return HttpResponse("Facebook Provider is not Configured")
    #account does not belong to the logged in user
    else:
        return HttpResponse("You are not allowed to view this Account")


@login_required
def post_widget(request):
    if request.method == 'POST':
        post = request.POST.get('post', '')
        if not post:
            return HttpResponse('You did not enter any message to post')
        post_to_list = request.POST.getlist('post_to_list')
        if not post_to_list:
            return HttpResponse('You did not select any account to post to')

        for post_to in post_to_list:
            provider = post_to.split(':')
            if provider[0] == 'facebook':
                post_to_facebook(post, request.user.id, provider[1])
            elif provider[0] == 'twitter':
                post_to_twitter(post, request.user.id, provider[1])
            else:
                pass

        return HttpResponse("Message has been posted")
    else:
        belongs = SocialAccount.objects.filter(user_id=request.user.id)
        context = {
            'accounts': belongs,
        }
        return render(request,'post.html', context)


def post_to_twitter(post, user_id, account_id):
    #check if this account belongs to the logged in user
    try:
        belongs = SocialAccount.objects.get(id=account_id, user_id=user_id)
        if belongs:
            app = SocialApp.objects.get(provider='twitter')
            #Check if Twitter Provider is configured
            if app:
                token = SocialToken.objects.get(app_id=app.id, account_id=account_id)
                #check if Twitter Auth token objects are configured for this account
                if token:
                    #create the twitter object from twython
                    twitter = Twython(app.client_id, app.secret, token.token, token.token_secret)
                    #TODO: Make this at most 140 characters and implement tweetlong
                    twitter.update_status(status=post[0:139])
    except Exception:
        # TODO: Create exception handling
        pass


def post_to_facebook(post, user_id, account_id):
    # check if this account belongs to the logged in user
    try:

        belongs = SocialAccount.objects.get(id=account_id, user_id=user_id)
        if belongs:
            app = SocialApp.objects.get(provider='facebook')
            # Check if Facebook Provider is configured
            if app:
                token = SocialToken.objects.get(app_id=app.id, account_id=account_id)
                facebook = OpenFacebook(access_token=token.token)
                facebook.set('me/feed', message=post)
    except Exception:
        # TODO: Create exception handling
        pass

def mine_facebook_data(d):
    listofobjects = []
    for message in d['data']:
        dict = {
        'username' : message['from']['name'],
        'FB_org_id' : message['from']['id'],
        'location' : str(message['place']) if 'place' in message else '',
        'actions': str(message['actions']) if 'actions' in message else '',
        'link': message['link'] if 'link' in message else '',
        'link_name' : message['name'] if 'name' in message else '',
        'caption' : message['caption'] if 'caption'in message else '',
        'link_description': message['description'] if 'description' in message else '',
        'num_shares': message['shares']['count'] if 'shares' in message else '',
        'content' : message['message'].replace('\n', '') if 'message' in message else '',
        'last_comment' : message['updated_time'],
        'published_date' : human_readable_time(dateparser.parse(message['created_time'])),
        'type' : message['type'],
        'message_id' : message['id'],
        'picture' : message['picture'] if 'picture' in message else '',
        'video_source': message['source'] if 'source' in message else '',
        }
        listofobjects.append(dict)

    return listofobjects


def human_readable_time(date_time):
    """
    converts a python datetime object to the
    format "X days, Y hours ago"

    @param date_time: Python datetime object

    @return:
        fancy datetime:: string
    """
    current_datetime = datetime.datetime.utcnow().replace(tzinfo = pytz.utc)
    delta = str(current_datetime - date_time)
    if delta.find(',') > 0:
        days, hours = delta.split(',')
        days = int(days.split()[0].strip())
        hours, minutes = hours.split(':')[0:2]
    else:
        hours, minutes = delta.split(':')[0:2]
        days = 0
    days, hours, minutes = int(days), int(hours), int(minutes)
    datelets =[]
    years, months, xdays = None, None, None
    plural = lambda x: 's' if x!=1 else ''
    if days >= 365:
        years = int(days/365)
        datelets.append('%d year%s' % (years, plural(years)))
        days = days%365
    if days >= 30 and days < 365:
        months = int(days/30)
        datelets.append('%d month%s' % (months, plural(months)))
        days = days%30
    if not years and days > 0 and days < 30:
        xdays =days
        datelets.append('%d day%s' % (xdays, plural(xdays)))
    if not (months or years) and hours != 0:
        datelets.append('%d hour%s' % (hours, plural(hours)))
    if not (xdays or months or years):
        datelets.append('%d minute%s' % (minutes, plural(minutes)))
    return ', '.join(datelets) + ' ago.'