from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.
class SocialMedia(models.Model):
    name = models.CharField(max_length=128, unique=True)
    api_url = models.CharField(max_length=500, default="")
    app_key = models.CharField(max_length=500, default="")
    app_secret = models.CharField(max_length=500, default="")

    def __unicode__(self):
        return self.name


class UserProfiles(models.Model):
    social_media = models.ForeignKey(SocialMedia)
    users = models.ForeignKey(User)
    is_active = models.BooleanField(default=True)
    social_media_access_token = models.CharField(max_length=500)
    created = models.DateTimeField()
    modified = models.DateTimeField()

    def __unicode__(self):
        return self.social_media.name


class Post(models.Model):
    post = models.CharField()
    user = models.ForeignKey(User)
    post_to = models.CharField()
    created = models.DateTimeField(default=datetime.now())

    def __unicode__(self):
        return self.post